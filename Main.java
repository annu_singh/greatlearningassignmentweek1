package com.greatLearning.assignment;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AdminDepartment ad = new AdminDepartment();
		
		//Admin Department Functionalities
				System.out.println(ad.deparmentName());
				System.out.println(ad.getTodaysWork());
				System.out.println(ad.getWorkDeadline());
				System.out.println(ad.isTodayAHoliday());
				System.out.println("---------------------------------------------------------");
		
	    HrDepartment hd = new HrDepartment();
       
		//HR Department Functionalities
				System.out.println(hd.deparmentName());
				System.out.println(hd.getTodaysWork());
				System.out.println(hd.getWorkDeadline());
				System.out.println(hd.getdoActivity());
				System.out.println(hd.isTodayAHoliday());
				System.out.println("---------------------------------------------------------");
		TechDepartment td = new TechDepartment();
	
		//Tech Department Functionalities
				System.out.println(td.departmentName());
				System.out.println(td.getTodaysWork());
				System.out.println(td.getWorkDeadline());
				System.out.println(td.getTechStackInformation());
				System.out.println(td.isTodayAHoliday());
				System.out.println("---------------------------------------------------------");
		
		
	}

}
